module.exports = {
  port: parseInt(process.env.PORT || 3000),
  mongoDsn: `mongodb://${process.env.DB_DSN || 'mongo'}/xara`
}
