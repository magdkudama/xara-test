const validator = require('./validator')

module.exports = function () {
  return require('./service')(validator)
}
