const Company = require('../db/schemas/company')
const Workspace = require('../db/schemas/workspace')
const User = require('../db/schemas/user')
const uuid = require('uuid/v1')

function CompanyService (validator) {
  this.validator = validator
}

CompanyService.prototype.addCompany = function (company, fn) {
  if (company) {
    company.name = company.displayName ? company.displayName.toLowerCase() : null
  }

  return this.validator.validateCompany(company, function (err) {
    if (err) return fn(err)
    const toInsert = new Company({
      _id: uuid(),
      displayName: company.displayName,
      name: company.name
    })

    return toInsert.save(function (err, result) {
      if (!err) return fn(null, { id: result.id, displayName: result.displayName })
      return fn('INTERNAL')
    })
  })
}

CompanyService.prototype.editCompany = function (id, company, fn) {
  if (company) {
    company.id = id
    company.name = company.displayName ? company.displayName.toLowerCase() : null
  }

  const me = this
  return Company.findById(id, function (err) {
    if (err) return fn('NOT_FOUND')

    return me.validator.validateCompany(company, function (err) {
      if (err) return fn(err)

      return Company.findOneAndUpdate(
        { _id: company.id },
        { $set: { displayName: company.displayName, name: company.name } },
        { new: true },
        function (err, result) {
          if (!err) return fn(null, { id: result.id, displayName: result.displayName })
          return fn('INTERNAL')
        })
    })
  })
}

CompanyService.prototype.addWorkspace = function (workspace, fn) {
  if (workspace) {
    workspace.name = workspace.displayName ? workspace.displayName.toLowerCase() : null
  }

  return this.validator.validateWorkspace(workspace, function (err) {
    if (err) return fn(err)

    const toInsert = new Workspace({
      _id: uuid(),
      displayName: workspace.displayName,
      name: workspace.name
    })

    return Company.findOneAndUpdate(
      { _id: workspace.companyId },
      { $push: { workspaces: toInsert } },
      { new: true },
      function (err, result) {
        if (!err) {
          const lastInserted = result.workspaces[result.workspaces.length - 1]
          return fn(null, { id: lastInserted._id, displayName: lastInserted.displayName })
        }
        return fn('INTERNAL')
      })
  })
}

CompanyService.prototype.editWorkspace = function (id, workspace, fn) {
  if (workspace) {
    workspace.id = id
    workspace.name = workspace.displayName ? workspace.displayName.toLowerCase() : null
  }

  return this.validator.validateWorkspace(workspace, function (err) {
    if (err) return fn(err)

    return Company.findOneAndUpdate(
      { _id: workspace.companyId, 'workspaces._id': workspace.id },
      { $set: { 'workspaces.$.name': workspace.name, 'workspaces.$.displayName': workspace.displayName } },
      { new: true },
      function (err, result) {
        if (err) return fn(err)
        const workspaceFound = result.workspaces.find(function (e) {
          return e._id === workspace.id
        })
        return fn(null, { id: workspaceFound._id, displayName: workspaceFound.displayName })
      })
  })
}

CompanyService.prototype.addUserToWorkspace = function (user, fn) {
  return this.validator.validateUser(user, function (err) {
    if (err) return fn(err)

    const toInsert = new User({
      _id: uuid(),
      email: user.email,
      role: user.role
    })

    return Company.findOneAndUpdate(
      { _id: user.companyId, 'workspaces._id': user.workspaceId },
      { $push: { 'workspaces.$.users': toInsert } },
      { new: true },
      function (err, result) {
        if (err) return fn(err)
        const workspace = result.workspaces.find(function (e) {
          return e.id === user.workspaceId
        })
        const userFound = workspace.users[workspace.users.length - 1]
        return fn(null, {
          id: userFound.id,
          role: userFound.role,
          email: userFound.email
        })
      })
  })
}

CompanyService.prototype.removeUserFromWorkspace = function (user, fn) {
  return this.validator.validateUserDeletion(user, function (err) {
    if (err) return fn(err)
    return Company.findOneAndUpdate(
      { _id: user.companyId, 'workspaces._id': user.workspaceId },
      { $pull: { 'workspaces.$.users': { _id: user.id } } },
      { safe: true },
      function (err) {
        if (err) return fn(err)
        return fn()
      })
  })
}

module.exports = function (validator) {
  return new CompanyService(validator)
}
