const Joi = require('joi')
const Company = require('../db/schemas/company')

function Validator () {
  this.userSchema = Joi.object().keys({
    companyId: Joi.string().required(),
    workspaceId: Joi.string().required(),
    email: Joi.string().email().required(),
    role: Joi.string().valid([ 'basic', 'admin' ]).required()
  })

  this.userDeletionSchema = Joi.object().keys({
    id: Joi.string().required(),
    companyId: Joi.string().required(),
    workspaceId: Joi.string().required()
  })

  this.workspaceSchema = Joi.object().keys({
    id: Joi.string(),
    companyId: Joi.string().required(),
    displayName: Joi.string().required(),
    name: Joi.string().required()
  })

  this.companySchema = Joi.object().keys({
    id: Joi.string(),
    displayName: Joi.string().required(),
    name: Joi.string().required()
  })
}

Validator.prototype.validateCompany = function (company, fn) {
  const result = Joi.validate(company, this.companySchema)

  if (result.error !== null) {
    return fn('VALIDATION')
  }

  return Company.findCompaniesByName(company.name, function (err, companies) {
    if (err) return fn('INTERNAL')

    const ids = companies.map(function (e) { return e._id })
    if (ids.length === 0) return fn()
    if (!company.id) return fn('VALIDATION')
    if (ids.length > 1 || ids.indexOf(company.id) === -1) return fn('CONFLICT')

    return fn()
  })
}

Validator.prototype.validateWorkspace = function (workspace, fn) {
  if (!workspace) return fn('VALIDATION')

  const v = Joi.validate(workspace, this.workspaceSchema)
  if (v.error !== null) return fn('VALIDATION')

  return Company.findWorkspacesWithNameInCompany(workspace.companyId, workspace.name, function (err, results) {
    if (err || !results) return fn('NOT_FOUND')
    if (results.length === 0) return fn()
    if (!workspace.id) return fn('CONFLICT')

    if (results.indexOf(workspace.id) !== -1) return fn()
    return fn('CONFLICT')
  })
}

Validator.prototype.validateUser = function (user, fn) {
  if (!user) return fn('VALIDATION')

  const v = Joi.validate(user, this.userSchema)
  if (v.error !== null) return fn('VALIDATION')

  return Company.findUserWithEmailInCompany(user.companyId, user.workspaceId, user.email, function (err, results) {
    if (err || !results) return fn('NOT_FOUND')
    if (results.length === 0) return fn()
    if (!user.id) return fn('CONFLICT')

    if (results.indexOf(user.id) !== -1) return fn()
    return fn('CONFLICT')
  })
}

Validator.prototype.validateUserDeletion = function (user, fn) {
  if (!user) return fn('VALIDATION')

  const v = Joi.validate(user, this.userDeletionSchema)
  if (v.error !== null) return fn('VALIDATION')

  return Company.existsUser(user.companyId, user.workspaceId, user.id, function (err, exists) {
    return fn((err || !exists) ? 'NOT_FOUND' : undefined)
  })
}

module.exports = new Validator()
