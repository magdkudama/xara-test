const express = require('express')
const config = require('./config')
const db = require('./db')
const log = require('./log')
const service = require('./service')(db)
const app = express()
const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())

function typeToStatusCode (type) {
  switch (type) {
    case 'VALIDATION':
      return 422
    case 'INTERNAL':
      return 500
    case 'NOT_FOUND':
      return 404
    case 'CONFLICT':
      return 409
    default:
      return 400
  }
}

app.post('/company', function (req, res) {
  service.addCompany(req.body, function (err, data) {
    if (!err) {
      res.status(201)
      return res.json(data)
    }

    res.status(typeToStatusCode(err))
    return res.json()
  })
})

app.put('/company/:id', function (req, res) {
  service.editCompany(req.params.id, req.body, function (err, data) {
    if (!err) {
      res.status(200)
      return res.json(data)
    }

    res.status(typeToStatusCode(err))
    return res.json()
  })
})

app.post('/company/:id/workspace', function (req, res) {
  const workspace = req.body
  workspace.companyId = req.params.id
  service.addWorkspace(workspace, function (err, data) {
    if (!err) {
      res.status(201)
      return res.json(data)
    }

    res.status(typeToStatusCode(err))
    return res.json()
  })
})

app.put('/company/:c/workspace/:w', function (req, res) {
  const workspace = req.body
  workspace.companyId = req.params.c
  service.editWorkspace(req.params.w, workspace, function (err, data) {
    if (!err) {
      res.status(200)
      return res.json(data)
    }

    res.status(typeToStatusCode(err))
    return res.json()
  })
})

app.post('/company/:c/workspace/:w/user', function (req, res) {
  const user = req.body
  user.companyId = req.params.c
  user.workspaceId = req.params.w

  service.addUserToWorkspace(user, function (err, data) {
    if (!err) {
      res.status(200)
      return res.json(data)
    }

    res.status(typeToStatusCode(err))
    return res.json()
  })
})

app.delete('/company/:c/workspace/:w/user/:u', function (req, res) {
  const user = {
    id: req.params.u,
    companyId: req.params.c,
    workspaceId: req.params.w
  }
  service.removeUserFromWorkspace(user, function (err, data) {
    if (!err) {
      res.status(204)
      return res.json(data)
    }

    res.status(typeToStatusCode(err))
    return res.json()
  })
})

app.use(function (err, req, res, next) {
  log.error(err.message)
  res.status(500)
  return res.json()
})

app.listen(config.port, function () {
  log.debug(`Application listening on port ${config.port}`)
})
