const db = require('../')

const userSchema = new db.Schema({
  _id: { type: String, required: true },
  email: { type: String, required: true, index: true },
  role: { type: String, required: true }
})

module.exports = db.model('user', userSchema)
