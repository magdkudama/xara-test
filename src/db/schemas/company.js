const db = require('../')
const workspace = require('./workspace')

const companySchema = new db.Schema({
  _id: { type: String, required: true },
  displayName: { type: String, required: true },
  name: { type: String, required: true, index: { unique: true } },
  workspaces: [ workspace.schema ]
})

companySchema.statics = {
  findCompaniesByName (name, cb) {
    return this.find({ name: name }, cb)
  },
  findWorkspacesByCompany (id, cb) {
    return this.findOne({ _id: id }, 'workspaces', cb)
  },
  findWorkspacesWithNameInCompany (companyId, workspaceName, cb) {
    return this.aggregate([
      {
        $match: { _id: companyId }
      },
      {
        $project: {
          elements: { $filter: { input: '$workspaces', as: 'w', cond: { $eq: [ '$$w.name', workspaceName ] } } }
        }
      },
      { $project: { 'elements._id': 1 } }
    ], function (err, results) {
      if (err) return cb(err)
      if (results.length === 0) return cb(new Error('Not found'))
      return cb(null, results[0].elements.map(function (e) {
        return e._id
      }))
    })
  },
  findUserWithEmailInCompany (companyId, workspaceId, email, cb) {
    return this.aggregate([
      {
        $match: {
          _id: companyId,
          'workspaces._id': workspaceId
        }
      },
      { $unwind: '$workspaces' },
      {
        $project: {
          elements: { $filter: { input: '$workspaces.users', as: 'u', cond: { $eq: [ '$$u.email', email ] } } }
        }
      },
      { $project: { 'elements._id': 1 } }
    ], function (err, results) {
      if (err) return cb(err)
      if (results.length === 0) return cb(new Error('Not found'))
      return cb(null, results[0].elements.map(function (e) {
        return e._id
      }))
    })
  },
  existsUser (companyId, workspaceId, userId, cb) {
    return this.aggregate([
      {
        $match: {
          _id: companyId,
          'workspaces._id': workspaceId,
          'workspaces.users._id': userId
        }
      },
      { $unwind: '$workspaces' },
      { $unwind: '$workspaces.users' },
      { $project: { user: '$workspaces.users._id', _id: 0 } }
    ], function (err, result) {
      if (err) return cb(err)
      if (result.length === 0) return cb(new Error('Not found'))
      const userFound = result.find(function (e) {
        return e.user === userId
      })

      if (userFound) return cb(null, true)
      else return cb(null, false)
    })
  }
}

module.exports = db.model('company', companySchema)
