const db = require('../')
const user = require('./user')

const workspaceSchema = new db.Schema({
  _id: { type: String, required: true },
  displayName: { type: String, required: true },
  name: { type: String, required: true, index: true },
  users: [ user.schema ]
})

module.exports = db.model('workspace', workspaceSchema)
