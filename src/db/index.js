const db = require('mongoose')
const config = require('../config')

db.connect(config.mongoDsn)

module.exports = db
