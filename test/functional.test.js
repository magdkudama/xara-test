const expect = require('chai').expect
const axios = require('axios')

describe('Functional tests', function () {
  let api

  function createCompany (name = 'Testing') {
    return api.post('/company', { displayName: name })
  }

  function createWorkspace (companyId, name = 'Testing') {
    return api.post(`/company/${companyId}/workspace`, { displayName: name })
  }

  function createUser (companyId, workspaceId, email = 'test@test.com', role = 'admin') {
    return api.post(`/company/${companyId}/workspace/${workspaceId}/user`, { email: email, role: role })
  }

  function deleteUser (companyId, workspaceId, userId) {
    return api.delete(`/company/${companyId}/workspace/${workspaceId}/user/${userId}`)
  }

  function createCompanyWithWorkspace (name = 'Testing', companyName = 'Testing') {
    let companyId
    return createCompany(companyName)
      .then(function (r) {
        companyId = r.data.id
        return createWorkspace(companyId, name)
      })
      .then(function (r) { return [ companyId, r ] })
  }

  function createCompanyWithWorkspaceAndUser () {
    let companyId
    let workspaceId
    return createCompanyWithWorkspace()
      .then(function (r) {
        companyId = r[0]
        workspaceId = r[1].data.id
        return createUser(companyId, workspaceId)
      })
  }

  function editCompany (id) {
    return api.put(`/company/${id}`, { displayName: 'Testing edit' })
  }

  function editWorkspace (companyId, workspaceId) {
    return api.put(`/company/${companyId}/workspace/${workspaceId}`, { displayName: 'Testing edit' })
  }

  before('Prepare environment', function () {
    if (!process.env.FUNCTIONAL) {
      this.skip()
    } else {
      require('../src/server')
      api = axios.create({
        baseURL: 'http://127.0.0.1:3000',
        timeout: 1000,
        headers: { 'Content-Type': 'application/json' }
      })
    }
  })

  afterEach('Drop collections', function (done) {
    const company = require('../src/db/schemas/company')
    company.remove({}, function (err) {
      done()
    })
  })

  describe('Company endpoints', function () {
    before(function () {
      if (!process.env.FUNCTIONAL) {
        this.skip()
      }
    })

    it('should not process null company', function (done) {
      api.post('/company', null).catch(function (e) {
        expect(e.response.status).to.equal(422)
        done()
      })
    })

    it('should not process empty company', function (done) {
      api.post('/company', {}).catch(function (e) {
        expect(e.response.status).to.equal(422)
        done()
      })
    })

    it('should process company with data', function (done) {
      createCompany().then(function (r) {
        expect(r.status).to.equal(201)
        done()
      })
    })

    it('should not create duplicate company', function (done) {
      createCompany()
        .then(function () { return createCompany() })
        .catch(function (e) {
          expect(e.response.status).to.equal(422)
          done()
        })
    })

    it('should not edit company when duplicating name', function (done) {
      let companyId
      createCompany()
        .then(function (r) {
          companyId = r.data.id
          return createCompany('Testing edit')
        })
        .then(function (r) { return editCompany(companyId) })
        .catch(function (e) {
          expect(e.response.status).to.equal(409)
          done()
        })
    })
  })

  describe('Workspace endpoints', function () {
    before(function () {
      if (!process.env.FUNCTIONAL) {
        this.skip()
      }
    })

    it('should not process unknown company', function (done) {
      api.post('/company/5afb46130e8a95c8a277b33d/workspace', { displayName: 'A' }).catch(function (e) {
        expect(e.response.status).to.equal(404)
        done()
      })
    })

    it('should allow creating a workspace', function (done) {
      createCompanyWithWorkspace()
        .then(function (r) {
          expect(r[1].status).to.equal(201)
          done()
        })
    })

    it('should not duplicate aleady existing workspace', function (done) {
      createCompanyWithWorkspace()
        .then(function (r) {
          return createWorkspace(r[0])
        })
        .catch(function (e) {
          expect(e.response.status).to.equal(409)
          done()
        })
    })

    it('should allow multiple workspaces inside a company', function (done) {
      createCompanyWithWorkspace()
        .then(function (r) {
          return createWorkspace(r[0], 'New Testing')
        })
        .then(function (r) {
          expect(r.status).to.equal(201)
          done()
        })
    })

    it('should allow same workspace in multiple companies', function (done) {
      createCompanyWithWorkspace()
        .then(function (r) {
          return createCompanyWithWorkspace('Testing 2', 'Testing 2')
        })
        .then(function (r) {
          expect(r[1].status).to.equal(201)
          done()
        })
    })

    it('should allow editing a workspace', function (done) {
      let companyId, workspaceId
      createCompanyWithWorkspace()
        .then(function (r) {
          companyId = r[0]
          workspaceId = r[1].data.id

          return editWorkspace(companyId, workspaceId)
        })
        .then(function (r) {
          expect(r.status).to.equal(200)
          done()
        })
    })
  })

  describe('User endpoints', function () {
    before(function () {
      if (!process.env.FUNCTIONAL) {
        this.skip()
      }
    })

    it('should allow creating users', function (done) {
      createCompanyWithWorkspaceAndUser()
        .then(function (r) {
          expect(r.status).to.equal(200)
          done()
        })
    })

    it('should allow deleting users', function (done) {
      let companyId
      let workspaceId
      createCompanyWithWorkspace()
        .then(function (r) {
          companyId = r[0]
          workspaceId = r[1].data.id
          return createUser(companyId, workspaceId)
        })
        .then(function (r) {
          return deleteUser(companyId, workspaceId, r.data.id)
        })
        .then(function (r) {
          expect(r.status).to.equal(204)
          done()
        })
    })

    it('should not allow deleting invalid users', function (done) {
      deleteUser('test', 'test', 'test')
        .catch(function (e) {
          expect(e.response.status).to.equal(404)
          done()
        })
    })

    it('should not allow create duplicate users (with same email)', function (done) {
      let companyId
      let workspaceId
      createCompanyWithWorkspace()
        .then(function (r) {
          companyId = r[0]
          workspaceId = r[1].data.id
          return createUser(companyId, workspaceId)
        })
        .then(function (r) { return createUser(companyId, workspaceId) })
        .catch(function (e) {
          expect(e.response.status).to.equal(409)
          done()
        })
    })
  })
})
