const expect = require('chai').expect
const path = require('path')
const filePath = '../src/config'

describe('Config file tests', function () {
  beforeEach('Unset env variables', function () {
    delete process.env.PORT
    delete process.env.DB_DSN

    // Clear NodeJS cache
    delete require.cache[path.join(__dirname, '../src/config.js')]
  })

  it('should read port from env variable', function () {
    process.env.PORT = 5000
    const config = require(filePath)
    expect(config.port).to.equal(5000)
  })

  it('should get the default port if no env variable present', function () {
    const config = require(filePath)
    expect(config.port).to.equal(3000)
  })

  it('should read mongo dsn from env variable', function () {
    process.env.DB_DSN = 'test-dsn'
    const config = require(filePath)
    expect(config.mongoDsn).to.equal('mongodb://test-dsn/xara')
  })

  it('should get the default mongo dsn if no env variable present', function () {
    const config = require(filePath)
    expect(config.mongoDsn).to.equal('mongodb://mongo/xara')
  })
})
