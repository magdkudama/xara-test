const expect = require('chai').expect
const proxyquire = require('proxyquire').noCallThru()
const filePath = '../src/service/validator'
const path = '../db/schemas/company'

describe('Validator for workspaces tests', function () {
  describe('Invalid workspaces', function () {
    it('null value', function (done) {
      const v = proxyquire(filePath, { [path]: {} })
      v.validateWorkspace(null, function (r) {
        expect(r).to.equal('VALIDATION')
        done()
      })
    })

    it('empty object', function (done) {
      const v = proxyquire(filePath, { [path]: {} })
      v.validateWorkspace({}, function (r) {
        expect(r).to.equal('VALIDATION')
        done()
      })
    })

    it('non-existing company', function (done) {
      const mock = {
        findWorkspacesWithNameInCompany: function (companyId, workspaceName, fn) {
          return fn('err')
        }
      }

      const v = proxyquire(filePath, { [path]: mock })
      v.validateWorkspace({ companyId: '12345', displayName: 'test', name: 'test' }, function (r) {
        expect(r).to.equal('NOT_FOUND')
        done()
      })
    })
  })

  describe('Valid workspace data', function () {
    describe('Inserting', function () {
      const validWorkspace = { displayName: 'Test', name: 'test', companyId: '12345' }

      it('should accept when empty workspaces', function (done) {
        const mock = {
          findWorkspacesWithNameInCompany: function (companyId, workspaceName, fn) {
            return fn(null, [])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateWorkspace(validWorkspace, function (r) {
          expect(r).to.be.undefined
          done()
        })
      })

      it('should not accept when other workspace has the same name', function (done) {
        const mock = {
          findWorkspacesWithNameInCompany: function (companyId, workspaceName, fn) {
            return fn(null, ['123456'])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateWorkspace(validWorkspace, function (r) {
          expect(r).to.equal('CONFLICT')
          done()
        })
      })
    })

    describe('Updating', function () {
      const validWorkspace = { id: '12345', displayName: 'Test', name: 'test', companyId: '12345' }

      it('should accept when there is no workspaces with the same name', function (done) {
        const mock = {
          findWorkspacesWithNameInCompany: function (companyId, workspaceName, fn) {
            return fn(null, [])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateWorkspace(validWorkspace, function (r) {
          expect(r).to.be.undefined
          done()
        })
      })

      it('should not accept when another workspace has the same name', function (done) {
        const mock = {
          findWorkspacesWithNameInCompany: function (companyId, workspaceName, fn) {
            return fn(null, ['2'])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateWorkspace(validWorkspace, function (r) {
          expect(r).to.equal('CONFLICT')
          done()
        })
      })

      it('should accept when the workspace itself (saved one) has the same name', function (done) {
        const mock = {
          findWorkspacesWithNameInCompany: function (companyId, workspaceName, fn) {
            return fn(null, ['12345'])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateWorkspace(validWorkspace, function (r) {
          expect(r).to.be.undefined
          done()
        })
      })
    })
  })
})
