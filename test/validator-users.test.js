const expect = require('chai').expect
const proxyquire = require('proxyquire').noCallThru()
const filePath = '../src/service/validator'
const path = '../db/schemas/company'

describe('Validator for users tests', function () {
  describe('Invalid users', function () {
    it('null value', function (done) {
      const v = proxyquire(filePath, { [path]: {} })
      v.validateUser(null, function (r) {
        expect(r).to.equal('VALIDATION')
        done()
      })
    })

    it('empty object', function (done) {
      const v = proxyquire(filePath, { [path]: {} })
      v.validateUser({}, function (r) {
        expect(r).to.equal('VALIDATION')
        done()
      })
    })

    it('not an email', function (done) {
      const v = proxyquire(filePath, { [path]: {} })
      v.validateUser({ email: 'not-email', role: 'admin' }, function (r) {
        expect(r).to.equal('VALIDATION')
        done()
      })
    })

    it('not a valid role', function (done) {
      const v = proxyquire(filePath, { [path]: {} })
      v.validateUser({ companyId: '123', workspaceId: '12345', email: 'test@test.com', role: 'adminNO' }, function (r) {
        expect(r).to.equal('VALIDATION')
        done()
      })
    })
  })

  describe('Valid user data', function () {
    describe('Inserting', function () {
      const validUser = { companyId: '123', workspaceId: '12345', email: 'test@test.com', role: 'admin' }

      it('should accept when empty users', function (done) {
        const mock = {
          findUserWithEmailInCompany: function (companyId, workspaceId, email, fn) {
            return fn(null, [])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateUser(validUser, function (r) {
          expect(r).to.be.undefined
          done()
        })
      })

      it('should not accept when duplicating email', function (done) {
        const mock = {
          findUserWithEmailInCompany: function (companyId, workspaceId, email, fn) {
            return fn(null, ['1234'])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateUser(validUser, function (r) {
          expect(r).to.equal('CONFLICT')
          done()
        })
      })
    })

    describe('Deleting', function () {
      const validUser = { companyId: '123', workspaceId: '12345', id: '12' }

      it('should not accept when deleting with invalid data', function (done) {
        const mock = {
          existsUser: function (companyId, workspaceId, userId, fn) {
            return fn(null, false)
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateUserDeletion({ companyId: '123', workspaceId: '12345' }, function (r) {
          expect(r).to.equal('VALIDATION')
          done()
        })
      })

      it('should not accept when deleting non-existing user', function (done) {
        const mock = {
          existsUser: function (companyId, workspaceId, userId, fn) {
            return fn(null, false)
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateUserDeletion(validUser, function (r) {
          expect(r).to.equal('NOT_FOUND')
          done()
        })
      })

      it('should accept when user exists', function (done) {
        const mock = {
          existsUser: function (companyId, workspaceId, userId, fn) {
            return fn(null, true)
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateUserDeletion(validUser, function (r) {
          expect(r).to.be.undefined
          done()
        })
      })
    })
  })
})
