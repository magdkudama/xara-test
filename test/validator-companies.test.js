const expect = require('chai').expect
const proxyquire = require('proxyquire').noCallThru()
const filePath = '../src/service/validator'
const path = '../db/schemas/company'

describe('Validator for companies tests', function () {
  describe('Invalid companies', function () {
    it('null value', function (done) {
      const v = proxyquire(filePath, { [path]: {} })
      v.validateCompany(null, function (r) {
        expect(r).to.equal('VALIDATION')
        done()
      })
    })

    it('empty object', function (done) {
      const v = proxyquire(filePath, { [path]: {} })
      v.validateCompany({}, function (r) {
        expect(r).to.equal('VALIDATION')
        done()
      })
    })
  })

  describe('Valid companies data', function () {
    describe('Inserting', function () {
      const validCompany = { displayName: 'Test', name: 'test' }

      it('db error', function (done) {
        const mock = {
          findCompaniesByName: function (name, fn) {
            return fn('DB error')
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateCompany(validCompany, function (r) {
          expect(r).to.equal('INTERNAL')
          done()
        })
      })

      it('should accept if no other companies with the same name', function (done) {
        const mock = {
          findCompaniesByName: function (name, fn) {
            return fn(null, [])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateCompany(validCompany, function (r) {
          expect(r).to.be.undefined
          done()
        })
      })

      it('should not accept if other companies have the same name', function (done) {
        const mock = {
          findCompaniesByName: function (name, fn) {
            return fn(null, [{ id: '12345' }])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateCompany(validCompany, function (r) {
          expect(r).to.equal('VALIDATION')
          done()
        })
      })
    })

    describe('Updating', function () {
      const validCompany = { id: '1', displayName: 'Test', name: 'test' }

      it('db error', function (done) {
        const mock = {
          findCompaniesByName: function (name, fn) {
            return fn('DB error')
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateCompany(validCompany, function (r) {
          expect(r).to.equal('INTERNAL')
          done()
        })
      })

      it('should accept when no other companies have that name', function (done) {
        const mock = {
          findCompaniesByName: function (name, fn) {
            return fn(null, [])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateCompany(validCompany, function (r) {
          expect(r).to.be.undefined
          done()
        })
      })

      it('should accept when there is a company with the same name, and it is the company being updated', function (done) {
        const mock = {
          findCompaniesByName: function (name, fn) {
            return fn(null, [{ _id: '1' }])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateCompany(validCompany, function (r) {
          expect(r).to.be.undefined
          done()
        })
      })

      it('should not accept when the new company name is used by another company', function (done) {
        const mock = {
          findCompaniesByName: function (name, fn) {
            return fn(null, [{ id: '12345' }])
          }
        }

        const v = proxyquire(filePath, { [path]: mock })
        v.validateCompany(validCompany, function (r) {
          expect(r).to.equal('CONFLICT')
          done()
        })
      })
    })
  })
})
