iXara Back-End Test
===================

## Project dependencies (non-dev)

* express + body-parser: web framework, with body parser to allow access to `req.body` when using REST APIs
* joi: for validating incoming data
* mongoose: a simple ODM for MongoDB
* uuid: to generate UUID v1
* winston: for logging, not using it much, but it's the first thing I install on every single project (a logger)

## Installing the project

In order to get the project up and running, install the dependencies:

```
$ npm install
```

```
$ npm run lint

> xara-backend-role@1.0.0 lint /Users/magdkudama/Projects/xara
> eslint src/**
```

We're using the standard JS configuration for eslint, which you can have a look at [here](https://standardjs.com/). I didn't override any of the configuration. I believe it's something each company decides. But I do agree there should always be a set of rules defined, to avoid multiple types of coding styles at once in the codebase.

## Endpoints

```
POST /company
PUT /company/:id
POST /company/:id/workspace
PUT /company/:company/workspace/:workspace
POST /company/:company/workspace/:workspace/user
DELETE /company/:company/workspace/:workspace/user/:user
```

## Testing

If you only want to run unit tests:

```
$ npm run test
> xara-backend-role@1.0.0 test /Users/magdkudama/Projects/xara
> mocha --exit

  Config file tests
    ✓ should read port from env variable
    ✓ should get the default port if no env variable present
    ✓ should read mongo dsn from env variable
    ✓ should get the default mongo dsn if no env variable present

  Functional tests
    Company endpoints
      - should not process null company
      - should not process empty company
      - should process company with data
      - should not create duplicate company
      - should not edit company when duplicating name
    Workspace endpoints
      - should not process unknown company
      - should allow creating a workspace
      - should not duplicate aleady existing workspace
      - should allow multiple workspaces inside a company
      - should allow same workspace in multiple companies
      - should allow editing a workspace
    User endpoints
      - should allow creating users
      - should allow deleting users
      - should not allow deleting invalid users
      - should not allow create duplicate users (with same email)

  Validator for companies tests
    Invalid companies
      ✓ null value (68ms)
      ✓ empty object
    Valid companies data
      Inserting
        ✓ db error
        ✓ should accept if no other companies with the same name
        ✓ should not accept if other companies have the same name
      Updating
        ✓ db error
        ✓ should accept when no other companies have that name
        ✓ should accept when there is a company with the same name, and it is the company being updated
        ✓ should not accept when the new company name is used by another company

  Validator for users tests
    Invalid users
      ✓ null value
      ✓ empty object
      ✓ not an email
      ✓ not a valid role
    Valid user data
      Inserting
        ✓ should accept when empty users
        ✓ should not accept when duplicating email
      Deleting
        ✓ should not accept when deleting with invalid data
        ✓ should not accept when deleting non-existing user
        ✓ should accept when user exists

  Validator for workspaces tests
    Invalid workspaces
      ✓ null value
      ✓ empty object
      ✓ non-existing company
    Valid workspace data
      Inserting
        ✓ should accept when empty workspaces
        ✓ should not accept when other workspace has the same name
      Updating
        ✓ should accept when there is no workspaces with the same name
        ✓ should not accept when another workspace has the same name
        ✓ should accept when the workspace itself (saved one) has the same name


  30 passing (134ms)
  15 pending
```

There's no tests for the service class; I only added tests for the validator class and some functional tests to guarantee the API works fine.

If you want to run all tests, including functional tests:

```
$ npm run test-all
> xara-backend-role@1.0.0 test-all /Users/magdkudama/Projects/xara
> FUNCTIONAL=true npm run test

> xara-backend-role@1.0.0 test /Users/magdkudama/Projects/xara
> mocha --exit

  Config file tests
    ✓ should read port from env variable
    ✓ should get the default port if no env variable present
    ✓ should read mongo dsn from env variable
    ✓ should get the default mongo dsn if no env variable present

  Functional tests
    Company endpoints
      ✓ should not process null company
      ✓ should not process empty company
      ✓ should process company with data
      ✓ should not create duplicate company
      ✓ should not edit company when duplicating name
    Workspace endpoints
      ✓ should not process unknown company
      ✓ should allow creating a workspace
      ✓ should not duplicate aleady existing workspace
      ✓ should allow multiple workspaces inside a company
      ✓ should allow same workspace in multiple companies
      ✓ should allow editing a workspace
    User endpoints
      ✓ should allow creating users
      ✓ should allow deleting users
      ✓ should not allow deleting invalid users
      ✓ should not allow create duplicate users (with same email)

  Validator for companies tests
    Invalid companies
      ✓ null value
      ✓ empty object
    Valid companies data
      Inserting
        ✓ db error
        ✓ should accept if no other companies with the same name
        ✓ should not accept if other companies have the same name
      Updating
        ✓ db error
        ✓ should accept when no other companies have that name
        ✓ should accept when there is a company with the same name, and it is the company being updated
        ✓ should not accept when the new company name is used by another company

  Validator for users tests
    Invalid users
      ✓ null value
      ✓ empty object
      ✓ not an email
      ✓ not a valid role
    Valid user data
      Inserting
        ✓ should accept when empty users
        ✓ should not accept when duplicating email
      Deleting
        ✓ should not accept when deleting with invalid data
        ✓ should not accept when deleting non-existing user
        ✓ should accept when user exists

  Validator for workspaces tests
    Invalid workspaces
      ✓ null value
      ✓ empty object
      ✓ non-existing company
    Valid workspace data
      Inserting
        ✓ should accept when empty workspaces
        ✓ should not accept when other workspace has the same name
      Updating
        ✓ should accept when there is no workspaces with the same name
        ✓ should not accept when another workspace has the same name
        ✓ should accept when the workspace itself (saved one) has the same name


  45 passing (550ms)
```

This will run the tests expecting a local MongoDB instance. Changing it to use the Dockerised MongoDB should be really simple (actually, a matter of changing the file to add an environment variable with the `MONGO_DSN`.

If you don't want to install NodeJS / NPM in your machine, or don't want to install MongoDB, you can run the project using Docker:

```
$ docker-compose up -d
$ curl http://localhost:3000
```

This will run the tests first (shouldn't be done on a production environment really, but it's just for the test) and then run the application.

## Comments

As the code test explicitly stated, the micro-service is using ES5 with callbacks rather than using `Promises` or the `async` / `await` APIs, which would've improved the code readability.

I couldn't find any Docker images which used NodeJS 5.1, so I'm afraid I didn't do that. Anyway, it's a deprecated release, and it doesn't even look like it has LTS. I'm using the oldest version of Node available in DockerHub (v6). If you want to use v5 strictly, I can bake my own Docker image. The code should work.

The coding test says: `it should consist in at least two classes, client and server`. I'm not sure exaclty what the client means. Am I expected to deliver the client-side application for this? If that's the case, please let me know and I'll create that.

If you want to use PostMan to test the API, import into it the file `Xara.postman_collection.json`, run the project and check it's working.

## Possible improvements

* Move code to later versions of the ES standard (test required to use callbacks)
* Add tests for the service, even though it's not doing much (calling the validator and saving to Mongo), which is already tested
* Use tools to allow for better development experience (nodemon, for example)
* Depending on the API, maybe add error messages, not only HTTP error codes
* Some of the queries to MongoDB could be optimised by using aggregation (for example). But I prefer to keep queries simple, unless there's a real use-case for it (for example, if we know each workspace will have millions of users). Anyway, to prove I know how to work with the aggregation framework from MongoDB, I created some "complex" aggregation queries in the program
* Given there's no wait in Docker, it could be that the application starts up before MongoDB is ready. Could be fixed by adding a command in Docker compose that waits, which is the recommended way

If there's anything else I can help with, do not hesitate to contact me!
